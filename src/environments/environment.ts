// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDW4y0r2gLmsuV8P2flc0KxlFTlFyT3M2o",
    authDomain: "bigoode-4f1db.firebaseapp.com",
    projectId: "bigoode-4f1db",
    storageBucket: "bigoode-4f1db.appspot.com",
    messagingSenderId: "316281331317",
    appId: "1:316281331317:web:8ac6723926c39b81a2e23c",
    measurementId: "G-XFV42P4K4X"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
