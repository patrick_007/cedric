import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from 'src/Services/auth/authentification.service';
import {UserModel } from '../../Models/User.model';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'Bigoodeee';
   islog: string='';
   currentUser: UserModel=new UserModel('','','','','','',false);
   constructor(
     public auth: AuthentificationService
   ){

   }
  status(): boolean{
    if(localStorage.getItem("user")!=null){
      
     this.currentUser = JSON.parse(localStorage.getItem('user')!);
     console.log(this.currentUser);
     return true
    }else{
      return false
    }
  }
}
