import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Bigoodeee'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('Bigoodeee');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('Bigoodeee app is running!');
  });
});

/*==================== MENU SHOW & HIDDEN ====================*/

/*===== MENU SHOW =====*/
const showMenu = (toggleId: string, navId: string) => {
	const toggle = document.getElementById(toggleId);
	const nav = document.getElementById(navId);

	/* Validate if constant exists */
	if (toggle && nav) {
		toggle.addEventListener("click", () => {
			// Add show-menu class to the div wich nav-menu ID
			nav.classList.toggle("show-menu");
		});
	}
};

showMenu("nav-toggle", "nav-menu");

/*==================== HERO TOGGLE TABS ====================*/
function toggleTabs(tabsBtn: string, tabsContent: string) {
    const buttons = document.querySelectorAll(`.${tabsBtn}`);
    const content = document.querySelectorAll(`.${tabsContent}`);

    /* Validate if constant exists */
    if (buttons && content) {
        showTabsContent(buttons)
    }

    function showTabsContent(element: any[] | NodeListOf<Element>) {
        element.forEach((button) => {
            button.addEventListener("click", () => {

                //Remove btn-active class to the first button 
                button.parentNode.querySelector('.btn-active').classList.remove('btn-active')

                //Add active class to current button tabs element 
                if (!button.classList.contains('btn-active')) button.classList.add('btn-active')
                
                //Get the id of the current button
                let buttonID = button.id;

                content.forEach((item) => {

                    // validate if the current tabs content have the id that match wich the current click button id
                    if (item.classList.contains(`${buttonID}`)) {
                        //Show the tabs content of the element who have the match ID
                        item.classList.add("tabs-active");
                    } else {
                        item.classList.remove("tabs-active");
                    }
                });
            });
        })
    }

}
    


toggleTabs("tabs-btn", "tabs-content");

