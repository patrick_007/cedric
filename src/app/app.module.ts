import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './modules/general/home/home.component';
import { BoutiqueComponent } from './modules/general/boutique/boutique.component';
import { LocalisationComponent } from './modules/general/localisation/localisation.component';
import { CoiffuresComponent } from './modules/general/coiffures/coiffures.component';
import { AideComponent } from './modules/general/aide/aide.component';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ConnexionComponent } from './modules/general/connexion/connexion.component';
import { InscriptionComponent } from './modules/general/inscription/inscription.component';
import { ForgotComponent } from './modules/general/forgot/forgot.component';
import { NewpassordComponent } from './modules/general/newpassord/newpassord.component';
import { NoustrouverComponent } from './modules/general/noustrouver/noustrouver.component';
import { ParisComponent } from './modules/general/paris/paris.component';
import { NantesComponent } from './modules/general/nantes/nantes.component';
import { RennesComponent } from './modules/general/rennes/rennes.component';
import { BordeauxComponent } from './modules/general/bordeaux/bordeaux.component';
import { LilleComponent } from './modules/general/lille/lille.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrCustumService } from 'src/Services/Toastr/toastr.service';
import { AuthentificationService } from 'src/Services/auth/authentification.service';
import { UserService } from 'src/Services/User/user.service';
import { CommunFunctionService } from 'src/Services/CommunFunctions/commun-functions.service';
import { GlobalParameter } from 'CONFIG_APP/init_Application';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BoutiqueComponent,
    LocalisationComponent,
    CoiffuresComponent,
    AideComponent,
    ConnexionComponent,
    InscriptionComponent,
    ForgotComponent,
    NewpassordComponent,
    NoustrouverComponent,
    ParisComponent,
    NantesComponent,
    RennesComponent,
    BordeauxComponent,
    LilleComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
    AngularFirestoreModule, 
    AngularFireAuthModule, 
    AngularFireStorageModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [ToastrCustumService, AuthentificationService, UserService, CommunFunctionService, GlobalParameter],
  bootstrap: [AppComponent]
})
export class AppModule { }
