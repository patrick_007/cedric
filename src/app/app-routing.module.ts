import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './modules/general/home/home.component';
import { BoutiqueComponent } from './modules/general/boutique/boutique.component';
import { CoiffuresComponent } from './modules/general/coiffures/coiffures.component';
import { LocalisationComponent } from './modules/general/localisation/localisation.component';
import { AideComponent } from './modules/general/aide/aide.component';
import { ConnexionComponent } from './modules/general/connexion/connexion.component';
import { InscriptionComponent } from './modules/general/inscription/inscription.component';
import { ForgotComponent } from './modules/general/forgot/forgot.component';
import { NewpassordComponent } from './modules/general/newpassord/newpassord.component';
import { NoustrouverComponent } from './modules/general/noustrouver/noustrouver.component';
import { ParisComponent } from './modules/general/paris/paris.component';
import { NantesComponent } from './modules/general/nantes/nantes.component';
import { RennesComponent } from './modules/general/rennes/rennes.component';
import { BordeauxComponent } from './modules/general/bordeaux/bordeaux.component';
import { LilleComponent } from './modules/general/lille/lille.component';



const routes: Routes = [
  { path : '', component: HomeComponent ,},
  { path : 'acceuil', component: HomeComponent ,},
  { path : 'boutique', component: BoutiqueComponent ,},
  { path : 'coiffeuses', component: CoiffuresComponent ,},
  { path : 'localisation', component: LocalisationComponent ,},
  { path : 'aide', component: AideComponent ,},
  { path : 'connexion', component: ConnexionComponent ,},
  { path : 'inscription', component: InscriptionComponent ,},
  { path : 'forgot', component: ForgotComponent ,},
  { path : 'newpassord', component: NewpassordComponent ,},
  { path : 'noustrouver', component: NoustrouverComponent,},
  { path : 'paris', component: ParisComponent,},
  { path : 'rennes', component: RennesComponent,},
  { path : 'nantes', component: NantesComponent,},
  { path : 'bordeaux', component: BordeauxComponent,},
  { path : 'lille', component: LilleComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
