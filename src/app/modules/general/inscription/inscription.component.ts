import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserModel} from "../../../../../Models/User.model";
import {UserService} from "../../../../Services/User/user.service";
import {Router} from "@angular/router";
import { AuthentificationService } from 'src/Services/auth/authentification.service';
import { CommunFunctionService } from 'src/Services/CommunFunctions/commun-functions.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  user: UserModel = new UserModel('','','','','','',false);
  users: UserModel[] = [];
  

  isEmailGood!: boolean;
  isPhoneGood!: boolean;
  isPaswordGood!: boolean;
  isValidationGood!: boolean;
  isButtonConnectClicked!: boolean;

  errorMessage!: string;
  userGroup: FormGroup=this.fb.group({
    displayName:[this.user.displayName,Validators.required],
    role:'client',
    email:[this.user.email,Validators.required],
    telephone:[this.user.telephone,Validators.required],
    password:[this.user.password,Validators.required],
    // ville:[this.user.ville],
    // biographie:[this.user.biographie],
  });
  
  

  constructor(private fb : FormBuilder,
              private  doctorServ : UserService,
              private router : Router,
              public currentServ: CommunFunctionService,
              public app: AppComponent,
              public authserv : AuthentificationService) {
  }

  ngOnInit() {
    this.initForm();
  }

  initForm(){

    this.userGroup= this.fb.group({
      displayName:[this.user.displayName,Validators.required],
      role:'client',
      email:[this.user.email,[Validators.required, Validators.email]],
      telephone:[this.user.telephone,Validators.required],
      password:[this.user.password,Validators.required],
      // ville:[this.user.ville],
      // biographie:[this.user.biographie],
    });
    this.isButtonConnectClicked = false;
    this.isEmailGood = true;
    this.isPhoneGood = true;
    this.isPaswordGood = true;
    this.isValidationGood = true;
  }

 async onCreate(){

    const data = this.userGroup.value;
    this.isButtonConnectClicked=true;
    console.log(data);
    const newUser = new UserModel(data.nom, data.email, data.telephone, 'client', undefined, undefined, false, data.password,'','' );;
     await this.isValidData(newUser).then(resultSet=> {
       if (resultSet===true)
       {
         this.doctorServ.add(newUser).subscribe(async (data) => {
           if(data)
           {
             await this.authserv.SignOut();
             await this.authserv.SignIn(newUser.email,newUser.password);
             await this.currentServ.redirectTo('acceuil');
           }
         }
        //  ,error=>{
        //    if(error.message.ToString().includes("phone"))
        //    {
        //      this.isPaswordGood=false;
        //      this.isPaswordGood=true;
        //      this.isEmailGood=true;
        //      this.isButtonConnectClicked=true;
        //      this.errorMessage='Verifiez le format du numero de téléphone !!!'
        //    }
          
        //  }
         );
       }
     }) ; 
  }

  isValidData(client: UserModel) {
    return new Promise(resolved => {
      if(client.email === '') {
        this.isEmailGood = false;
        this.errorMessage = 'veuillez entrer une adresse mail ';
        resolved(false);
       } else if(client.telephone === '+33' || client.telephone === '') {
        this.isPhoneGood = false;
        this.isPaswordGood = true;
        this.isEmailGood = true;
        this.errorMessage = 'Verifiez votre numero de téléphone'
        resolved(false);
      }else if(client.password === '') {
         this.isPaswordGood = false;
         this.isPhoneGood = true;
          this.isEmailGood = true;
         this.errorMessage = 'Veuillez entrer un mot de passe ';
         resolved(false);
       }
        else {
        resolved(true);
       }
    });
  }

}
