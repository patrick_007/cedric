import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthentificationService } from 'src/Services/auth/authentification.service';
import { ToastrCustumService } from 'src/Services/Toastr/toastr.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  active_tab_one : string | undefined;
  active_tab_two!: string;
  isSearchActiveTab!: boolean;
  listClientes!: [number,number, number];
  IsNotDeployed!: boolean;
  isCardOver!: boolean;
  connectForm!: FormGroup;
  isEmailGood!: boolean;
  ispasswordGood!: boolean;
  isConnectButtonClicked!: boolean;
  errorEmailMessage! : string;
  errorPasswordMessage! : string;

  constructor(private toastr : ToastrCustumService,private formBuilder: FormBuilder, public authserv : AuthentificationService,private router: Router
    ) {
    
  }

  ngOnInit(): void {
    this.activeTab(1);
    this.initClassVar();
  }


  async SeConnecter(groupConection: any) {
    await this.authserv.SignIn(groupConection.email, groupConection.password).then(async (result: any) => {
       if(result && result.field === 'email') {
         this.isConnectButtonClicked = true;
         this.isEmailGood = false;
         this.errorEmailMessage = result.message;
       }
       if(result && result.field === 'password') {
        this.isConnectButtonClicked = true;
        this.ispasswordGood = false;
        this.isEmailGood = true;
        this.errorPasswordMessage = result.message;
      }
      this.toastr.montrerleToastDeSucces('vous êtes bien connectés !')
    }).catch(error => {
      debugger;
    });
  }

  redirectTo(chemin: string) {
    // this.router.navigateByUrl(chemin);
  }

   /**
   * 
   * @param indexTab 
   */
    activeTab(indexTab: number) {
      if(indexTab && indexTab === 1) {
        this.active_tab_one = "active";
        this.active_tab_two = "";
        this.isSearchActiveTab = true;
      } else {
        this.active_tab_one = "";
        this.active_tab_two = "active";
        this.isSearchActiveTab = false;
      }
    }
  
    async initClassVar() {
      this.listClientes = [1,1,1];
      this.IsNotDeployed = true;
      this.isCardOver = false;
      this.isConnectButtonClicked = false;
      this.isEmailGood = true;
      this.connectForm = this.formBuilder.group({ 
        email: ['', [Validators.required, Validators.email]],
        password:  ['', [Validators.required, Validators.maxLength(40)]],
      });
    }
  
    changeButtonMenu(indexButton : number) {
      if(indexButton === 1) {
        this.IsNotDeployed = false;
      }
      else {
        this.IsNotDeployed = true;
      }
    }
  
    activateButton() {
      this.isCardOver = true;
    }

}
