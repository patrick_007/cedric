import { TestBed } from '@angular/core/testing';

import { ToastrCustumService } from './toastr.service';

describe('ToastrService', () => {
  let service: ToastrCustumService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ToastrCustumService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
