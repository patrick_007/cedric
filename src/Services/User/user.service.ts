import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserModel} from "../../../Models/User.model";
import { GlobalParameter } from 'CONFIG_APP/init_Application';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  baseUrl = "";
  Users: UserModel[]=[];
  isAuth: boolean=false;

  constructor( private param: GlobalParameter, private http: HttpClient) { 
    this.baseUrl = this.param.ServeurApi + "Users";
 }
  add (user: UserModel) {
      return this.http.post(`${this.baseUrl}`, user);
  }

  getAll(){
    return this.http.get(`${this.baseUrl}/all`);
  }

    getSingle(uid: string){
        return this.http.get(`${this.baseUrl}/uid/${uid}`);
    }

    update(user: UserModel, uid:string) {
        return this.http.post(`${this.baseUrl}/update/${uid}`, user);
    }

    // delete(uid: string){
    //     return this.http.delete(`${this.baseUrl}/delete/${uid}`);
    // }

    // loadData(): void{
    //     this.getAll().subscribe((data: UserModel[]) =>{
    //         if(data){
    //             this.Users = data;
    //         } else {
    //           this.Users = [];
    //         }
    //     });
    // }

  // connect(login: string , password: string) {
  //   return this.http.post(`${this.baseUrl}/login` ,
  //       {login: login , password:password});
  // }


}
