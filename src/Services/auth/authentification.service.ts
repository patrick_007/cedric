import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth'
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { UserModel } from '../../../Models/User.model';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  public userData: any; // Save logged in user data
  isUserConnected!: boolean;

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,  
    public ngZone: NgZone // NgZone service to remove outside scope warning
  ) { 

    /* Saving user data in localstorage when 
    logged in and setting up null when logged out */
    this.isUserConnected = false;
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        this.isUserConnected = true;
        localStorage.setItem('user', JSON.stringify(this.userData));
      } else {
        // localStorage.setItem('user',);
        // JSON.parse(localStorage.getItem('user'));
      }
    });
  }

  // Sign in with email/password
  async SignIn(email: string , password: string ) {
    return new Promise(async resolved => {
      await this.afAuth.signInWithEmailAndPassword(email, password)
      .then(result => {
        this.ngZone.run(() => {
          this.router.navigate(['acceuil']);
        });
        this.SetUserData(result.user);  
        this.isUserConnected = true;
        resolved(true);
      }).catch(error => {
        this.setErrorMessage(error).then(resulTraitement => {
          resolved(resulTraitement);
        });
    });
    })
  }

  setErrorMessage(error: any) {
    return new Promise(resolved => {
      if (error && error.code === 'auth/user-not-found') {
        const object = {
          field : "email",
          message: "il n'existe pas d'utilisateur avec cet adresse"
        }
        resolved(object);
      }
      if (error && error.code === 'auth/wrong-password') {
        const object = {
          field : "password",
          message: "le Mot de passe est incorect !!! "
        }
        resolved(object);
      }
    });
   
  }

  // Sign out 
  async SignOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigateByUrl('acceuil');
    });
  }

  SetUserData(user: any) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: UserModel = new UserModel(user.displayName, user.email, user.phoneNumber, 'client', user.uid,
    user.photoURL, user.emailVerified, user.password);
    this.userData = userData;
    localStorage.setItem('user', JSON.stringify(userData));
    return userRef.set(userData, {
       merge: true
    });
  }

   
}
