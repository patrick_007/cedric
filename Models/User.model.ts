

export class UserModel {

       uid?: string;
    displayName: string;
    email: string; 
    password: string; 
    role: string;
    photoURL?: string;
    emailVerified?: boolean;
    telephone: string;
     ville ?: string;
     biographie ?: string;

  
    constructor(displayName: string, email: string, telephone: string, role: string, uid?: string, photoURL?: string, emailVerified?:boolean, password?: string , ville ?: string,
         biographie ?: string,) {
        this.displayName = displayName;
        this.email = email,
        this.password = password ? password : "hidden";
        this.telephone = "+237" + telephone;
        this.role = role;
        this.uid = uid ? uid : undefined;
        this.photoURL = photoURL ? photoURL : undefined;
        this.ville=ville? ville : undefined;
        this.biographie=biographie? biographie :undefined;
        this.emailVerified = emailVerified ? emailVerified : false;
    }

}
